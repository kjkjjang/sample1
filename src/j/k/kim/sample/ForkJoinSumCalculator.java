package j.k.kim.sample;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.LongStream;

public class ForkJoinSumCalculator extends RecursiveTask<Long>{

	private static final long serialVersionUID = -8489960126587275005L;
	
	private final long[] numbers;
	private final int start;
	private final int end;
	
	private String node;
	
	public static final long THRESHOLD = 10_000;
	
	public ForkJoinSumCalculator(long[] numbers, String node) {
		this(numbers, 0, numbers.length, node);
	}

	public ForkJoinSumCalculator(long[] numbers, int start, int end, String node) {
		this.numbers = numbers;
		this.start = start;
		this.end = end;
		this.node = node;
		System.out.println("Forked[" + node + "]");
	}
	
	@Override
	protected Long compute() {
		int length = end - start;
		if (length < THRESHOLD) {
			return computeSequentially();
		}
		ForkJoinSumCalculator leftTask = 
				new ForkJoinSumCalculator(numbers, start, start + length / 2, node + "|left");
		leftTask.fork();
		ForkJoinSumCalculator rightTask = 
				new ForkJoinSumCalculator(numbers, start + length / 2, end, node + "|right");
		Long rightResult = rightTask.compute();
		Long leftResult = leftTask.join();
		
		return leftResult + rightResult;
	}

	private Long computeSequentially() {
		long sum = 0;
		for (int i = start; i < end; i++) {
			sum += numbers[i];
		}
		return sum;
	}
	
	public static long forkJoinSum(long n) {
		long[] numbers = LongStream.rangeClosed(1, n).toArray();
		ForkJoinTask<Long> task = new ForkJoinSumCalculator(numbers, "root");
		long result = new ForkJoinPool().invoke(task);
		return result;
	}
	

	public static void main(String[] args) {
		long n = 10_000_000;
		long[] numbers = LongStream.rangeClosed(1, n).toArray();
		ForkJoinTask<Long> task = new ForkJoinSumCalculator(numbers, "root");
		long result = new ForkJoinPool().invoke(task);
		System.out.println(result);
	}
}
