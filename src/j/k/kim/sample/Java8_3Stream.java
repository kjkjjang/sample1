package j.k.kim.sample;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.maxBy;
import static java.util.Comparator.comparingInt;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


public class Java8_3Stream {
	
	static class Dish{
		private final String name;
		private final boolean vegetarian;
		private final int calories;
		private final DishType type;
		
		public Dish(String name, boolean vegetarian, int calories, DishType type) {
			this.name = name;
			this.vegetarian = vegetarian;
			this.calories = calories;
			this.type = type;
		}
		
		public String getName() {
			return name;
		}



		public boolean isVegetarian() {
			return vegetarian;
		}



		public int getCalories() {
			return calories;
		}

		public DishType getType() {
			return type;
		}

	}

	public enum DishType {
		MEAT, FISH, OTHER
	}

	public static void main(String[] args) {
		List<Dish> menu = Arrays.asList(
			new Dish("pork", false, 800, DishType.MEAT),
			new Dish("beef", false, 700, DishType.MEAT),
			new Dish("chicken", false, 400, DishType.MEAT),
			new Dish("french fries", true, 530, DishType.OTHER),
			new Dish("rice", true, 350, DishType.OTHER),
			new Dish("season fruit", true, 120, DishType.OTHER),
			new Dish("pizza", true, 550, DishType.OTHER),
			new Dish("prawns", false, 300, DishType.FISH),
			new Dish("salmon", false, 450, DishType.FISH)
		);

		long dishesCount1 = menu.stream().count();
		long dishesCount2 = menu.stream().collect(Collectors.counting());
		
		Optional<Dish> maxCaloricDish = menu.stream().collect(Collectors.maxBy(Comparator.comparingInt(Dish::getCalories)));
		System.out.println(maxCaloricDish.get().getName());
		Optional<Dish> minCaloricDish = menu.stream().collect(Collectors.minBy(Comparator.comparingInt(Dish::getCalories)));
		System.out.println(minCaloricDish.get().getName());
		
		System.out.println();

		int totalCalories = menu.stream().collect(Collectors.reducing(0, Dish::getCalories, (i, j) -> i + j));
		System.out.println(totalCalories);
		
		System.out.println();
		
		List<String> over300CaloricDishNames = 
				menu.stream()
				.filter(d -> d.getCalories() > 300)
				.map(Dish::getName)
//				.limit(3)
				.collect(Collectors.toList());
		
		over300CaloricDishNames.stream()
			.forEach(dishName -> System.out.println(dishName));
		
		System.out.println();
		
		List<String> threeHighCaloricDishNames = 
				menu.stream()
				.sorted(Comparator.comparing(Dish::getCalories).reversed())
				.map(d -> d.getName())
				.limit(3)
				.collect(Collectors.toList());
		threeHighCaloricDishNames.stream()
			.forEach(d -> System.out.println(d));
		
		System.out.println();
		
		Map<DishType, List<Dish>> dishesByType = 
				menu.stream().collect(Collectors.groupingBy(Dish::getType));
		dishesByType.forEach((DishType dishType, List<Dish> dishList) 
				-> System.out.println("type[" + dishType + "]List[" + dishList.stream().map(Dish::getName).collect(Collectors.joining(",") ) + "]"));
		
		Map<DishType, Long> dishesTypeCount = 
				menu.stream().collect(groupingBy(Dish::getType, counting()));
		dishesTypeCount.forEach((DishType dishType, Long count) -> System.out.println("Type[" + dishType + "]Count[" + count + "]"));
		
		System.out.println();
		
		Map<DishType, Dish> mostCaloricByType = 
				menu.stream().collect(groupingBy(Dish::getType, 
						collectingAndThen(maxBy(comparingInt(Dish::getCalories)), Optional::get)));
		mostCaloricByType.forEach((DishType dishType, Dish dish) -> System.out.println("Type[" + dishType + "]dish[" + dish.getName() + "]"));
		
		System.out.println();
		
		Map<Boolean, List<Dish>> partitionedMenu = 
				menu.stream().collect(Collectors.partitioningBy(Dish::isVegetarian));
		partitionedMenu.forEach((Boolean isVegi, List<Dish> dishList) -> System.out.println("IsVegiterian[" + isVegi + "]Dishes[" + dishList.stream().map(Dish::getName).collect(Collectors.joining(",")) + "]"));
		
		List<Dish> vegiterianDishes = 
				menu.stream().filter(Dish::isVegetarian).collect(Collectors.toList());
		vegiterianDishes.forEach(dish -> System.out.println(dish.getName()));
		
		System.out.println();
		
		
	}
}
