package j.k.kim.sample;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public class Java8_2_MethodReference {
	public static void main(String[] args) {
		Function<String, String> function = String::new;
		String hello = function.apply("Hello");
		
		System.out.println(hello);
		
		Supplier<Integer> tmpStr = hello::length;
		int strLength = tmpStr.get();
		
		System.out.println(strLength);
		
		Supplier<List<String>> list = ArrayList::new;
		List<String> emptyList = list.get();
	}
}
