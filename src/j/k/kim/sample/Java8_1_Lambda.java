package j.k.kim.sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class Java8_1_Lambda {
	public static void main(String[] args) {
		// Functional Interfaces
		// Predicate<T>		|	T -> boolean
		// Consumer<T>		|	T -> void
		// Function<T, R>	|	T -> R
		// Supplier<T>		|	() -> T
		// UnaryOperator<T>	|	T -> T
		// BinaryOperator<T>|	(T, T) -> T
		// BiPredicate<L,R> |	(L, R) -> boolean
		// BiConsumer<T,U>	|	(T, U) -> void
		// BiFunction<T,U,R>|	(T, U) -> R
		
	}
	
	// Predicate<T>		|	T -> boolean
	// IntPredicate, LongPredicate, DoublePredicate
	public static void samplePredicate() {
		IntPredicate evenNumbers = (int i) -> i % 2 == 0;
		evenNumbers.test(1000);
		
		IntPredicate oddNumbers = (int i) -> i % 2 == 1;
		oddNumbers.test(1000);
	}
	
	// Consumer<T>		|	T -> void
	// IntConsumer, LongConsumer, DoubleConsumer
	public static void sampleConsumer() {
		Consumer<Integer> intPrinter = (Integer i) -> System.out.println(i);
		intPrinter.accept(5);
		
		Consumer<String> strPrinter = (String s) -> System.out.println(s);
		strPrinter.accept("Hello");
	}
	
	// Function<T, R>	|	T -> R
	// IntFunction, LongFunction, DoubleFunction
	// IntToLongFunction, IntToDoubleFunction
	// LongToIntFunction, LongToDoubleFunction
	// ToIntFunction, ToLongFunction, ToDoubleFunction
	public static void sampleFunction() {
		Function<String, Integer> strLength = (String s) -> s.length();
		strLength.apply("Hello World");
		
		Function<BufferedReader, String> readLine = (BufferedReader br) -> { 
			try {
				return br.readLine();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		};
		readLine.apply(new BufferedReader(new InputStreamReader(System.in)));
	}
	
	// Supplier<T>		|	() -> T
	// BooleanSupplier, IntSupplier, LongSupplier, DoubleSupplier
	public static void sampleSupplier() {
		Supplier<ArrayList<String>> listSupplier = () -> new ArrayList<>();
		ArrayList<String> list = listSupplier.get();
		
		Supplier<Map<Boolean, List<Integer>>> mapper = () -> new HashMap<>();
		Map<Boolean, List<Integer>> map = mapper.get();
	}
	
	// UnaryOperator<T>	|	T -> T
	// IntUnaryOperator, LongUnaryOperator, DoubleUnaryOperator
	public static void sampleUnaryOperator() {
		UnaryOperator<Integer> multiply = (Integer i) -> i * i;
		multiply.apply(3);
		
		UnaryOperator<String> appendWorld = (String s) -> s + " World";
	}
	
	// BinaryOperator<T>|	(T, T) -> T
	public static void sampleBinaryOperator() {
		BinaryOperator<String> msgAppender = (String s1, String s2) -> s1 + " " + s2;
		msgAppender.apply("Hello", "World");
		
		BinaryOperator<Integer> sumOperator = (Integer i1, Integer i2) -> i1 + i2;
		sumOperator.apply(10, 20);
	}
	
	// BiPredicate<L,R> |	(L, R) -> boolean
	public static void sampleBiPredicate() {
		BiPredicate<String, Integer> isLongString = (String str, Integer size) -> str.length() > size;
		isLongString.test("Hello World", 10);
	}
	
	// BiConsumer<T,U>	|	(T, U) -> void
	public static void sampleBiConsumer() {
		BiConsumer<Integer, String> toStringAppend = (Integer i, String str) -> System.out.println(i + "|" + str);
		toStringAppend.accept(10, "Hello");
		
		BiConsumer<List<String>, Set<String>> addAll = 
				(List<String> list, Set<String> set) -> list.addAll(set);
		addAll.accept(new ArrayList<>(), new HashSet<>());
	}
	
	// BiFunction<T,U,R>|	(T, U) -> R
	public static void sampleBiFunction() {
		BiFunction<String, String, Integer> sumStrLengh = (String s1, String s2) -> s1.length() + s2.length();
		sumStrLengh.apply("hello", "world");
	}
}
