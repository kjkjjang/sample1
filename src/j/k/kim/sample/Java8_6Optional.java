package j.k.kim.sample;

import java.util.Optional;

public class Java8_6Optional {
	public static void main(String[] args) {
		// Empty Car
		Optional<Car> optEmptyCar = Optional.empty();
		
		Car car = new Car();
		
		// Exist Car
		Optional<Car> existCar = Optional.of(car);
		
		// If Car is null then Car is Null 
		// else Car is not null
		Optional<Car> optCar = Optional.ofNullable(car);
		
		Person p = new Person();
		Optional<Person> optPerson = Optional.of(p);
		String personName = 
			optPerson.flatMap(Person::getCar)
					.flatMap(Car::getInsurance)
					.map(Insurance::getName)
					.orElse("Unknown");
		System.out.println(personName);
		
	}
	
	static class Person {
		private Optional<Car> car;
		private Optional<Car> getCar() { return car; }
	}
	
	static class Car {
		private Optional<Insurance> insurance;
		public Optional<Insurance> getInsurance() { return insurance; }
	}
	
	static class Insurance {
		private String name;
		public String getName() {return name; }
	}
}
