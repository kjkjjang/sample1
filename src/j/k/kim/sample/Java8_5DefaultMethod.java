package j.k.kim.sample;

public class Java8_5DefaultMethod {
	static interface Sized {
		int size();
		default boolean isEmpty() {
			return size() == 0;
		}
	}
	
	
	static interface A {
		default void hello() {
			System.out.println("Hello A");
		}
	}
	
	static interface B extends A {
		default void hello() {
			System.out.println("Hello B");
		}
	}
	
	static class C implements A, B {
		public static void main(String[] args) {
			new C().hello();
		}
	}
}
