package j.k.kim.sample;

import java.util.function.Function;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Java8_4ParallelStream {
	public static void main(String[] args) {
		timeGab(Java8_4ParallelStream::parallelSumBeforeOptical, 10_000_000);
		timeGab(Java8_4ParallelStream::iterativeSum, 10_000_000);
		timeGab(Java8_4ParallelStream::sequentialSum, 10_000_000);
		timeGab(Java8_4ParallelStream::longRangeSum, 10_000_000);
		timeGab(Java8_4ParallelStream::parallelLongRangeSum, 10_000_000);
		
		timeGab(ForkJoinSumCalculator::forkJoinSum, 10_000_000);
	}
	
	public static void timeGab(Function<Long, Long> func, long t) {
		long before = System.nanoTime();
		long result = func.apply(t);
		long after = System.nanoTime();
		long diff = (after - before) / 1000 / 1000;
		System.out.println("Time[" + diff + "]Result[" + result + "]");
	}
	
	public static long sequentialSum(long n) {
		return Stream.iterate(1L, i -> i + 1)
				.limit(n)
				.reduce(0L, Long::sum);
	}
	
	public static long iterativeSum(long n) {
		long result = 0;
		for (long i = 1L; i <= n; i++) {
			result += i;
		}
		return result;
	}
	
	public static long parallelSumBeforeOptical(long n) {
		return Stream.iterate(1L, i -> i + 1)
					.limit(n)
					.parallel()
					.reduce(0L, Long::sum);
	}

	public static long longRangeSum(long n) {
		return LongStream.rangeClosed(1, n)
				.reduce(0L, Long::sum);
	}
	
	public static long parallelLongRangeSum(long n) {
		return LongStream.rangeClosed(1, n)
				.parallel()
				.reduce(0L, Long::sum);
	}
}
