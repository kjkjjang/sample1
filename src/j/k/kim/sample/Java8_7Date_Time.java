package j.k.kim.sample;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;

public class Java8_7Date_Time {
	public static void main(String[] args) {
		// 2015-03-20
		LocalDate date = LocalDate.of(2015, 3, 20);
		
		Month monthOfDate = date.getMonth();
		int len = date.lengthOfMonth();
		// 윤년
		boolean leap = date.isLeapYear();
		
		LocalDate now = LocalDate.now();
		int year = now.get(ChronoField.YEAR);
		int month = now.get(ChronoField.MONTH_OF_YEAR);
		int day = now.get(ChronoField.DAY_OF_MONTH);
		
		// 13:45:20 (hour, minute, second)
		LocalTime localTime = LocalTime.of(13, 45, 20);
		int hour = localTime.getHour();
		int minute = localTime.getMinute();
		int second = localTime.getSecond();
		
		LocalDate tmpDate = LocalDate.parse("2015-03-20");
		LocalTime tmpTime = LocalTime.parse("13:45:20");
		
		// 2014-03-18 13:34:20
		LocalDateTime dt1 = LocalDateTime.of(2014, Month.MARCH, 18, 13, 34, 20);
		LocalDateTime dt2 = LocalDateTime.of(now, localTime);
		LocalDateTime dt3 = now.atTime(13, 34, 20);
		LocalDateTime dt4 = now.atTime(localTime);
		LocalDateTime dt5 = localTime.atDate(now);
		
		Duration d1 = Duration.between(localTime, tmpTime);
		Duration d2 = Duration.between(now, tmpDate);
		Duration d3 = Duration.between(dt1, dt2);
		
		Duration threeMinutes1 = Duration.ofMinutes(3);
		Duration threeMinutes2 = Duration.of(3, ChronoUnit.MINUTES);
		
		Period tenDays = Period.ofDays(10);
		Period threeWeeks = Period.ofWeeks(3);
		Period twoYearsSixMonthsOneDay = Period.of(2, 6, 1);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDate = now.format(formatter);
		LocalDate formatDate = LocalDate.parse(formattedDate, formatter);
		
	}
}
