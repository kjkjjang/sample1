package j.k.kim.infinite_stream;

import java.util.stream.Stream;

public class Fibonachi {
	public static void main(String[] args) {
		// infinite stream 
		// fibonachi 1 -> array
		fibonachi1();
		
		// fibonachi 2 -> num
		fibonachi2();
	}

	private static void fibonachi2() {
		Stream.iterate(new int[]{0, 1},
				t -> new int[]{t[1], t[0] + t[1]})
			.skip(2)
			.limit(5)
			.map(t -> t[0])
			.forEach(t -> System.out.println("[" + t + "]") );
	}
	
	public static void fibonachi1() {
		Stream.iterate(new int[]{0,  1}, 
				t -> new int[]{t[1], t[0] + t[1]})
			.skip(2)
			.limit(5)
			.forEach(t -> System.out.println("[" + t[0] + "," + t[1] + "]") );
			//.forEach(System.out::println);		
	}
}
